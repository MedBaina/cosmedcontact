<?php

namespace Cosmed\CoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('CosmedContactCoreBundle:Default:index.html.twig', array('name' => $name));
    }
}
