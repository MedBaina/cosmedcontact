<?php

namespace Cosmed\UserBundle\Controller;

use FOS\UserBundle\Controller\SecurityController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class LoginController extends SecurityController
{
    /**
     * @Route("/login")
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        return parent::loginAction($request);
    }

    /**
     * Overwrites the parent class function to redefine login template
     *
     * @param array $data
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function renderLogin(array $data)
    {
        return $this->render('CosmedUserBundle:Security:login.html.twig', $data);
    }
}
