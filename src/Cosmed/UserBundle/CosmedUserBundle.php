<?php

namespace Cosmed\UserBundle;

use FOS\UserBundle\FOSUserBundle;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class CosmedUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
