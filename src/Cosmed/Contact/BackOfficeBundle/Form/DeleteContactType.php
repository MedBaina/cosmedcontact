<?php

namespace Cosmed\Contact\BackOfficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class DeleteContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('to_delete', 'entity', array(
                'class' => 'CosmedContactBackOfficeBundle:Contact',
                'data_class' => null,
                'choice_label' => 'id',
                'required' => false,
                'multiple' => true,
                'expanded' => true
            ))
            ->add('submit', 'submit', array(
                'label' => 'Confirmer'
            ));
    }

    public function getName()
    {
        return 'delete_contact_form';
    }
}