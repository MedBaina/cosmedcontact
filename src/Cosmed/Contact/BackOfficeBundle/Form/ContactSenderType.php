<?php

namespace Cosmed\Contact\BackOfficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ContactSenderType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email')
            ->add('civility')
            ->add('firstName')
            ->add('lastName')
            ->add('phone')
            ->add('birthDate')
            ->add('address')
            ->add('city')
            ->add('zipCode')
            ->add('company')
            ->add('country')
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cosmed\Contact\BackOfficeBundle\Entity\ContactSender'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'cosmed_contact_backofficebundle_contactsender';
    }
}
