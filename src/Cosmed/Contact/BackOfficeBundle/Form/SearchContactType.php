<?php

namespace Cosmed\Contact\BackOfficeBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SearchContactType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('from', 'date', array(
            'label' => 'De :',
            'format' => 'd/M/y',
            'input' => 'datetime',
            'widget' => 'single_text'
             ))
            ->add('to', 'date', array(
                'label' => 'A :',
                'format' => 'd/M/y',
                'input' => 'datetime',
                'widget' => 'single_text'))
            ->add('receiver', 'entity', array(
                'class' => 'CosmedContactBackOfficeBundle:ContactReceiver',
                'data_class' => 'Cosmed\Contact\BackOfficeBundle\Entity\ContactReceiver',
                'choice_label' => 'name',
                'placeholder' => 'Tous',
                'empty_data'  => null,
                'required' => false
            ))
            ->add('keyword', 'text', array(
                'label' => 'Mot clé :'
            ))
            ->add('submit', 'submit', array(
                'label' => 'Envoyer'
            ));
    }

    public function getName()
    {
        return 'search_contact_form';
    }
} 