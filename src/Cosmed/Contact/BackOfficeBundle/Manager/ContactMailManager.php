<?php

namespace Cosmed\Contact\BackOfficeBundle\Manager;

use Cosmed\Contact\BackOfficeBundle\Entity\Contact;
use FOS\UserBundle\Model\UserManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Swift_Mailer;
use Cosmed\Contact\BackOfficeBundle\Manager\ContactManager;
use Symfony\Bundle\TwigBundle\TwigEngine;

class ContactMailManager
{
    protected $mailer;
    protected $contact;
    protected $userManager;

    public function __construct(Swift_Mailer $mailer, UserManager $userManager, TwigEngine $twigEngine)
    {
        $this->userManager = $userManager;
        $this->mailer = $mailer;
        $this->twigEngine = $twigEngine;
    }

    /**
     *
     * @return string
     */
    protected  function getAdminMail()
    {
        return $this->userManager->findUserByUsername('admin')->getEmail();
    }

    protected function getMailSubject(Contact $contact)
    {
        return sprintf('Formulaire de contact : %s %s', $contact->getSender()->getLastName(), $contact->getSender()->getFirstName());
    }


    public function sendMail(Contact $contact)
    {
        $message = \Swift_Message::newInstance()
            ->setFrom('noreplay@domaine.com')
            ->setSubject($this->getMailSubject($contact))
            ->setTo($contact->getReceiver()->getEmail())
            ->setCc($this->getAdminMail())
            ->setBody(
                $this->twigEngine->render(
                    'CosmedContactBackOfficeBundle:Emails:contact.html.twig',
                    array('contact' => $contact)
                ),
                'text/html'
            );
        $this->mailer->send($message);
    }
}