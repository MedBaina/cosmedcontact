<?php

namespace Cosmed\Contact\BackOfficeBundle\Manager;

use  Liuggio\ExcelBundle\Factory as ExcelFactory;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;


class ExportContactManager
{
    CONST CREATOR = 'COSMED ADMIN';
    CONST DESCRIPTION = 'Extraction de Cosmed Contacts';

    protected $excelFactory;
    protected $contactHeader = array('Date', 'IP', 'Nom', 'Prénom',
        'Email', 'Tél', 'Ville', 'Destinataire');

    public function __construct(ExcelFactory $excelFactory)
    {
        $this->excelFactory = $excelFactory;
        $this->createExcelObj();
    }

    protected function createExcelObj()
    {
        $this->excelObj = $this->excelFactory->createPHPExcelObject();
        return $this;
    }

    protected function setFileProperties()
    {
        $this->excelObj->getProperties()
            ->setCreator(SELF::CREATOR)
            ->setDescription(SELF::DESCRIPTION);
        return $this;
    }

    protected function createFileHeader()
    {
        $abc = range('A', 'Z');
        $i = 0;
        foreach ($this->contactHeader as $name) {
            $this->excelObj->setActiveSheetIndex(0)
                ->setCellValue($abc[$i] . '1', $name);
            $i++;
        }
        return $this;
    }

    public function createContactsRows(\ArrayIterator $contacts)
    {
        //@todo define method names in variable and fill with magic methods
        $i = 2;
        foreach ($contacts as $contact) {
            $country =  (null !== $contact->getSender()->getCountry()) ? $contact->getSender()->getCountry()->getName() : '';
            $this->excelObj->setActiveSheetIndex(0)
                ->setCellValue('A' . $i, $contact->getDate())
                ->setCellValue('B' . $i, $contact->getIp())
                ->setCellValue('C' . $i, $contact->getSender()->getLastName())
                ->setCellValue('D' . $i, $contact->getSender()->getFirstName())
                ->setCellValue('E' . $i, $contact->getSender()->getEmail())
                ->setCellValue('F' . $i, $contact->getSender()->getPhone())
                ->setCellValue('G' . $i, $country)
                ->setCellValue('H' . $i, $contact->getReceiver()->getName());
            $i++;
        }

        return $this;
    }

    public function setSheetTitle($title = '')
    {
        $this->excelObj->getActiveSheet()->setTitle($title);
        // Set active sheet index to the first sheet, so Excel opens this as the first sheet
        $this->excelObj->setActiveSheetIndex(0);
        return $this;
    }

    public function setFileName($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    public function createFile(\ArrayIterator $contacts, $filename = 'extract', $sheetTitle = '')
    {
        return $this->createFileHeader()
            ->createContactsRows($contacts)
            ->setSheetTitle($sheetTitle)
            ->setFileName($filename);
    }

    public function renderFile()
    {
        // create the writer
        $writer = $this->excelFactory->createWriter($this->excelObj, 'Excel5');
        // create the response
        $response = $this->excelFactory->createStreamedResponse($writer);
        // adding headers
        $dispositionHeader = $response->headers->makeDisposition(
            ResponseHeaderBag::DISPOSITION_ATTACHMENT,
            $this->filename . '.xls'
        );

        $response->headers->set('Content-Type', 'application/vnd.ms-excel; charset=utf-8');
        $response->headers->set('Pragma', 'public');
        $response->headers->set('Cache-Control', 'maxage=1');
        $response->headers->set('Content-Disposition', $dispositionHeader);

        return $response;
    }
}
