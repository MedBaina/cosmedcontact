<?php

namespace Cosmed\Contact\BackOfficeBundle\Manager;

use Cosmed\Contact\BackOfficeBundle\Entity\Contact;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\DoctrineDbalAdapter;
use Pagerfanta\Adapter\DoctrineORMAdapter;

class ContactManager
{
    protected $objectManager;
    protected $class;
    protected $repository;

    /**
     * @param ObjectManager $om
     * @param $class
     */
    public function __construct(ObjectManager $om, $class)
    {
        $this->objectManager = $om;
        $this->repository = $om->getRepository($class);

        $metadata = $om->getClassMetadata($class);
        $this->class = $metadata->getName();
    }

    public function createContact(Contact $contact)
    {
        $this->objectManager->persist($contact);
        $this->objectManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function deleteContact(Contact $contact)
    {
        $this->objectManager->remove($contact);
        $this->objectManager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function deleteContacts(array $ids)
    {
        foreach ($ids as $id) {
            $this->objectManager->remove($this->objectManager->getReference($this->class, $id));
        }

        $this->objectManager->flush();
    }

    public function findContacts()
    {
        return $this->repository->findAll();
    }

    public function findContactsPaginatedAdapter()
    {
        $countQueryBuilderModifier = function ($queryBuilder) {
            $queryBuilder->select('COUNT(DISTINCT p.id) AS total_results')
                ->setMaxResults(1);
        };

        $queryBuilder = $this->objectManager->createQueryBuilder()
            ->select('c')
            ->from('CosmedContactBackOfficeBundle:Contact', 'c');

        return new DoctrineORMAdapter($queryBuilder, $countQueryBuilderModifier);
    }

    public function searchContactsAdapter($from = null, $to = null, $receiver = null, $keyword = null)
    {
        $countQueryBuilderModifier = function ($queryBuilder) {
            $queryBuilder->select('COUNT(DISTINCT p.id) AS total_results')
                ->setMaxResults(1);
        };

        $queryBuilder = $this->objectManager
            ->createQueryBuilder()
            ->select('c')
            ->from('CosmedContactBackOfficeBundle:Contact', 'c')
            ->join('c.sender', 'cs')
            ->join('c.receiver', 'cr')
            ->join('cs.country', 'country');

        if (null !== $from) {
            $queryBuilder->andWhere('c.date > :from')
                ->setParameter('from', $from->format('Y-m-d'));
        }

        if (null !== $to) {
            $queryBuilder->andWhere('c.date < :to')
                ->setParameter('to', $to->format('Y-m-d'));
        }

        if (null !== $receiver) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->like('cr.id', ':receiver'))
                ->setParameter('receiver', $receiver->getId());
        }

        if (null !== $keyword) {
            $queryBuilder->andWhere(
                $queryBuilder->expr()->like('c.date', ':keyword')
            )
                ->orWhere(
                    $queryBuilder->expr()->like('c.ip', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('c.message', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('cs.firstName', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('cs.lastName', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('cs.email', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('cs.phone', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('cs.city', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('cs.zipCode', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('cs.address', ':keyword')
                )
                ->orWhere(
                    $queryBuilder->expr()->like('cs.company', ':keyword')
                )

                ->orWhere(
                    $queryBuilder->expr()->like('country.name', ':keyword')
                );
            if (null == $receiver) {
                $queryBuilder->orWhere(
                    $queryBuilder->expr()->like('cr.firstName', ':keyword')
                )
                    ->orWhere(
                        $queryBuilder->expr()->like('cr.lastName', ':keyword')
                    )
                    ->orWhere(
                        $queryBuilder->expr()->like('cr.function', ':keyword')
                    )
                    ->orWhere(
                        $queryBuilder->expr()->like('cr.email', ':keyword')
                    );
            }
            $queryBuilder->setParameter('keyword', '%' . $keyword . '%');

        }

        return new DoctrineORMAdapter($queryBuilder, $countQueryBuilderModifier);
    }

    public function findContactsPaginated($page = 1, $perPage = 10)
    {
        return $this->objectManager
            ->createQuery('SELECT contact FROM CosmedContactBackOfficeBundle:Contact contact')
            ->setMaxResults($perPage)
            ->setFirstResult($page * $perPage)
            ->getResult();
    }

    /**
     * {@inheritDoc}
     */
    public function findContactBy(array $criteria)
    {
        return $this->repository->findOneBy($criteria);
    }


    public function countContacts()
    {
        return $this->objectManager
            ->createquery('SELECT count(c) FROM CosmedContactBackOfficeBundle:Contact c')
            ->getResult()['0']['1'];
    }
}