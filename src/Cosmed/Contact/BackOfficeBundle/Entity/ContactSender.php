<?php
namespace Cosmed\Contact\BackOfficeBundle\Entity;

use Cosmed\Contact\FrontOfficeBundle\Form\Type\ContactSenderType;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\UniqueConstraint;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * @ORM\Entity
 * @ORM\Table(name="contact_senders")
 */
class ContactSender
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     * @Assert\NotBlank(message= "error.email.required")
     * @Assert\Email(message= "error.email.format")
     * @Assert\Length(
     *      max = 150,
     *      maxMessage = "error.email.max"
     * )
     * @var string
     */
    protected $email;

    /**
     * @Assert\NotBlank(message= "error.civility.required")
     * @ORM\Column(type="string", length=3)
     */
    protected $civility;

    /**
     * @Assert\NotBlank(message= "error.first_name.required")
     * @Assert\Length(
     *      max = 30,
     *      maxMessage = "error.first_name.max"
     * )
     *
     * @ORM\Column(type="string", length=30)
     */
    protected $firstName;

    /**
     * @Assert\NotBlank(message= "error.last_name.required")
     * @Assert\Length(
     *      max = 30,
     *      maxMessage = "error.last_name.max"
     * )
     *
     * @ORM\Column(type="string", length=30)
     */
    protected $lastName;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $phone;

    /**
     * @ORM\Column(type="date")
     */
    protected $birthDate;

    /**
     * @Assert\Length(
     *      max = 150,
     *      maxMessage = "error.address.max"
     * )
     *
     * @ORM\Column(type="string", length=150)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", length=50)
     */
    protected $city;

    /**
     * @Assert\Length(
     *      max = 10,
     *      maxMessage = "error.zip_code.max"
     * )
     *
     * @ORM\Column(type="string", length=10)
     */
    protected $zipCode;

    /**
     * @ORM\ManyToOne(targetEntity="Country", fetch="EAGER")
     * @ORM\JoinColumn{name="country_id", referencedColumnName="id")
     */
    protected $country;

    /**
     * @Assert\Length(
     *      max = 150,
     *      maxMessage = "error.company.max"
     * )
     *
     * @ORM\Column(type="string", length=150)
     */
    protected $company;

    /**
     * @param mixed $address
     *
     * @return ContactSender
     */
    public function setAddress($address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param mixed $birthDate
     *
     * @return ContactSender
     */
    public function setBirthDate($birthDate)
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getBirthDate()
    {
        return $this->birthDate;
    }

    /**
     * @param mixed $city
     *
     * @return ContactSender
     */
    public function setCity($city)
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * @param mixed $civility
     *
     * @return ContactSender
     */
    public function setCivility($civility)
    {
        $this->civility = $civility;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCivility()
    {
        return $this->civility;
    }

    /**
     * @param mixed $company
     *
     * @return ContactSender
     */
    public function setCompany($company)
    {
        $this->company = $company;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @param mixed $country
     *
     * @return ContactSender
     */
    public function setCountry($country)
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * @param string $email
     *
     * @return ContactSender
     */
    public function setEmail($email)
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $firstName
     *
     * @return ContactSender
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @param mixed $id
     *
     * @return ContactSender
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $lastName
     *
     * @return ContactSender
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param mixed $phone
     *
     * @return ContactSender
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $zipCode
     *
     * @return ContactSenders
     */
    public function setZipCode($zipCode)
    {
        $this->zipCode = $zipCode;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getZipCode()
    {
        return $this->zipCode;
    }


}

