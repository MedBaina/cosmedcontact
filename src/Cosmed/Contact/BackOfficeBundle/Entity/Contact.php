<?php
namespace Cosmed\Contact\BackOfficeBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * @ORM\Table(name="cosmed_contacts")
 * @ORM\HasLifecycleCallbacks()
 */
class Contact
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $date;

    /**
     * @ORM\Column(type="string")
     */
    protected $ip;

    /**
     * @ORM\ManyToOne(targetEntity="ContactSender", cascade={"all"}, fetch="EAGER")
     * @ORM\JoinColumn{name="sender_id", referencedColumnName="id")
     * @Assert\Valid
     */
    protected $sender;

    /**
     * @Assert\NotBlank(message= "error.receiver.required")
     *
     * @ORM\ManyToOne(targetEntity="ContactReceiver", fetch="EAGER")
     * @ORM\JoinColumn{name="receiver_id", referencedColumnName="id")
     * @Assert\Valid
     */
    protected $receiver;

    /**
     * @Assert\NotBlank(message= "error.message.required")
     * @Assert\Length(
     *      max = 500,
     *      maxMessage = "error.message.max"
     * )
     * @ORM\Column(type="string", length=500)
     */
    protected $message;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $newsLetter;

    /**
     * @param mixed $id
     *
     * @return Contact
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     *
     *
     * @param mixed $date
     * @return Contact
     */
    public function setDate($date)
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param mixed $message
     * @return Contact
     */
    public function setMessage($message)
    {
        $this->message = $message;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * @param mixed $ip
     *
     * @return Contact
     */
    public function setIp($ip)
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param ContactSender $receiver
     * @return Contact
     */
    public function setReceiver($receiver)
    {
        $this->receiver = $receiver;
        return $this;
    }

    /**
     * @return ContactReceiver
     */
    public function getReceiver()
    {
        return $this->receiver;
    }

    /**
     * @param ContactSender $sender
     *
     * @return Contact
     */
    public function setSender($sender)
    {
        $this->sender = $sender;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getSender()
    {
        return $this->sender;
    }

    /**
     * @param mixed $newsLetter
     *
     * @return Contact
     */
    public function setNewsLetter($newsLetter)
    {
        $this->newsLetter = $newsLetter;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getNewsLetter()
    {
        return $this->newsLetter;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreationDate()
    {
        $this->date = new \DateTime();
    }

}
