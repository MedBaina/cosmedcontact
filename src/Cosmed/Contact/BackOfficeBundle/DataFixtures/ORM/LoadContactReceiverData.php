<?php

namespace Cosmed\UserBundle\DataFixtures\ORM;

use Cosmed\Contact\BackOfficeBundle\Entity\ContactReceiver;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class LoadContactReceiverData implements FixtureInterface, ContainerAwareInterface
{
    /**
     * @var ContainerInterface
     */
    private $container;

    /**
     * {@inheritDoc}
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function load(ObjectManager $manager)
    {
        $contactReceivers = array(
            array(
            'firstName' => 'Khalid',
            'lastName' => 'Bennani',
            'function' => 'Directeur commercial',
            'email'    => 'k.benani@cosmed.ma'
            ),
            array(
                'firstName' => 'Hania',
                'lastName' => 'Mourad',
                'function' => 'Responsable SAV',
                'email'    => 'h.mourad@cosmed.ma'
            ),
            array(
                'firstName' => 'Adil',
                'lastName' => 'Jouda',
                'function' => 'Responsable ressources humaines',
                'email'    => 'a.jouda@cosmed.ma'
            ));

        foreach ($contactReceivers as $receiver) {
            $contactReceiver = new ContactReceiver();
            $contactReceiver->setFirstName($receiver['firstName'])
                ->setLastName($receiver['lastName'])
                ->setEmail($receiver['email'])
                ->setFunction($receiver['function']);

            $manager->persist($contactReceiver);
            $manager->flush();
        }




    }
}