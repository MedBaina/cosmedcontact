<?php

namespace Cosmed\Contact\BackOfficeBundle\Controller;

use Cosmed\Contact\BackOfficeBundle\Form\DeleteContactType;
use Cosmed\Contact\BackOfficeBundle\Form\SearchContactType;
use Pagerfanta\Pagerfanta;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Cookie;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class AdminController extends Controller
{
    /**
     * @Route("/{page}",
     *     defaults={"page" = 1},
     *     requirements={"page" = "\d+"})
     * @Route("/")
     * @TempLate("CosmedContactBackOfficeBundle:Admin:admin.html.twig")
     *
     * @param $request
     * @param $page
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request, $page = 1)
    {
        $form = $this->createForm(new SearchContactType());
        $toDeleteForm = $this->createForm(new DeleteContactType());
        $data = null;

        if ('POST' === $request->getMethod()) {
            $form->handleRequest($request);
            if ($form->isValid()) {
                $data = $form->getData();
            }
        }

        $pager = $this->getContactsByFilters($data, $page);

        $cookie = new Cookie('contact_filter', serialize(array(
                'data' => $data,
                'page' => $page)
        ));

        $response = new Response();
        $response->headers->setcookie($cookie);
        $response->send();

        return array('contacts' => $pager,
            'form' => $form->createView(),
            'toDeleteForm' => $toDeleteForm->createView()
        );
    }

    /**
     * @Route("contacts/export")
     * @TempLate("CosmedContactBackOfficeBundle:Admin:admin.html.twig")
     *
     * @param Request $request
     */
    public function excelExportAction(Request $request)
    {
        $filters = $request->cookies->has('contact_filter') ? unserialize($request->cookies->get('contact_filter')) : null;

        $contacts = $this->getContactsByFilters($filters['data'], $filters['page'])
            ->getCurrentPageResults();
        $exportContainer = $this->container->get('contact_excel_export');

        return $exportContainer
            ->createFile($contacts, (new \DateTime())->format('dmYhis'), 'contacts')
            ->renderFile();

    }

    /**
     * @Route("/contact-detail/{id}")
     * @TempLate("CosmedContactBackOfficeBundle:Admin:contact_detail.html.twig")
     *
     * @param $id
     *
     * @return array
     */
    public function contactDetailAction($id)
    {
        $contactContainer = $this->container->get('back_office.contact_manager');
        $contact = $contactContainer->findContactBy(array('id' => $id));

        return array('contact' => $contact);
    }

    /**
     * @Route("/delete-contact")
     *
     */
    public function deleteContactAction(Request $request)
    {
        $msg = 'Aucune suppression n\'a été effectuée';
        $contactContainer = $this->container->get('back_office.contact_manager');
        if ($request->request->has('delete_contact_form')) {
            $data = $request->request->get('delete_contact_form');
            $contactContainer->deleteContacts($data['to_delete']);
            $msg = 'La suppression a été bien effectuée';
        }
        $this->addFlash(
            'notice',
            $msg
        );
        return $this->redirectToRoute('cosmed_contact_backoffice_admin_index');
    }

    /**
     * Retrieves Paginated contact filtered with search params (filters)
     *
     * @param $filters
     * @param $page
     * @param $maxPerPage
     * @return Pagerfanta
     */
    protected function getContactsByFilters($filters, $page = 1, $maxPerPage = 25)
    {
        if (null == $filters) {
            return $this->getContacts($page, $maxPerPage);
        }
        $contactContainer = $this->container->get('back_office.contact_manager');

        $pagerAdapter = $contactContainer->searchContactsAdapter(
            $filters['from'],
            $filters['to'],
            $filters['receiver'],
            $filters['keyword']
        );

        $pager = new Pagerfanta($pagerAdapter);
        $pager->setMaxPerPage($maxPerPage);
        $pager->setCurrentPage($page);

        return $pager;
    }

    /**
     * Retrieves all contacts with pagination
     *
     * @param $maxPerPage
     * @param $page
     * @return Pagerfanta
     */
    protected function getContacts($page = 1, $maxPerPage = 25)
    {
        $contactContainer = $this->container->get('back_office.contact_manager');
        $pagerAdapter = $contactContainer->findContactsPaginatedAdapter();

        $pager = new Pagerfanta($pagerAdapter);
        $pager->setMaxPerPage($maxPerPage);
        $pager->setCurrentPage($page);

        return $pager;
    }
}
