<?php

namespace Cosmed\Contact\BackOfficeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Cosmed\Contact\BackOfficeBundle\Entity\ContactReceiver;
use Cosmed\Contact\BackOfficeBundle\Form\ContactReceiverType;

/**
 * ContactReceiver controller.
 *
 * @Route("/receiver")
 */
class ContactReceiverController extends Controller
{

    /**
     * Lists all ContactReceiver entities.
     *
     * @Route("/", name="receiver")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('CosmedContactBackOfficeBundle:ContactReceiver')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new ContactReceiver entity.
     *
     * @Route("/", name="receiver_create")
     * @Method("POST")
     * @Template("CosmedContactBackOfficeBundle:ContactReceiver:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new ContactReceiver();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('receiver_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Creates a form to create a ContactReceiver entity.
     *
     * @param ContactReceiver $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(ContactReceiver $entity)
    {
        $form = $this->createForm(new ContactReceiverType(), $entity, array(
            'action' => $this->generateUrl('receiver_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new ContactReceiver entity.
     *
     * @Route("/new", name="receiver_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $entity = new ContactReceiver();
        $form   = $this->createCreateForm($entity);

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
        );
    }

    /**
     * Finds and displays a ContactReceiver entity.
     *
     * @Route("/{id}", name="receiver_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CosmedContactBackOfficeBundle:ContactReceiver')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactReceiver entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing ContactReceiver entity.
     *
     * @Route("/{id}/edit", name="receiver_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CosmedContactBackOfficeBundle:ContactReceiver')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactReceiver entity.');
        }

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
    * Creates a form to edit a ContactReceiver entity.
    *
    * @param ContactReceiver $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(ContactReceiver $entity)
    {
        $form = $this->createForm(new ContactReceiverType(), $entity, array(
            'action' => $this->generateUrl('receiver_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing ContactReceiver entity.
     *
     * @Route("/{id}", name="receiver_update")
     * @Method("PUT")
     * @Template("CosmedContactBackOfficeBundle:ContactReceiver:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('CosmedContactBackOfficeBundle:ContactReceiver')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ContactReceiver entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            $em->flush();

            return $this->redirect($this->generateUrl('receiver_edit', array('id' => $id)));
        }

        return array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }
    /**
     * Deletes a ContactReceiver entity.
     *
     * @Route("/{id}", name="receiver_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('CosmedContactBackOfficeBundle:ContactReceiver')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ContactReceiver entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('receiver'));
    }

    /**
     * Creates a form to delete a ContactReceiver entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('receiver_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
