<?php

namespace Cosmed\Contact\FrontOfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactType extends AbstractType
{
    private $class;

    /**
     * @param string $class The User class name
     */
    public function __construct($class)
    {
        $this->class = $class;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('sender', new ContactSenderType(),
            array('label' => false))
            ->add('receiver', 'entity', array(
                'class' => 'CosmedContactBackOfficeBundle:ContactReceiver',
                'data_class' => 'Cosmed\Contact\BackOfficeBundle\Entity\ContactReceiver',
                'property' => 'name',
                'placeholder' => 'Choisir',
                'empty_data'  => null,
                'label' => 'label.receiver'
            ))
            ->add('message', 'textarea', array(
                'label' => 'label.message',
                'attr' => array('cols' => 50, 'rows' => 10)
            ))
            ->add('newsLetter', 'checkbox', array(
                'label' => 'label.news_letter',
                'required' => false
            ))
            ->add('captcha', 'captcha', array(
                'label' => 'êtes-vous un humain ? ',
                'width' => '160',
                'height'=> '55',
                'invalid_message' => "veuillez entrer les caractères sur l'image ci-après",
            ))
            ->add('submit', 'submit', array(
                'label' => 'label.submit'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
            'intention' => 'cosmed_contact',
            'translation_domain' => 'cosmed_contact_form',
            'csrf_protection' => false
        ));
    }

    public function getName()
    {
        return 'cosmed_contact';
    }
} 