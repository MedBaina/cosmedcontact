<?php

namespace Cosmed\Contact\FrontOfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactSenderType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('civility', 'choice',
            array(
                'label' => 'label.civility',
                'choices' =>
                    array('options.civility.mr' => 'options.civility.mr',
                        'options.civility.mme' => 'options.civility.mme',
                        'options.civility.mlle' => 'options.civility.mlle'),
                'placeholder' => 'Choisir',
                'empty_data'  => null
            ))
            ->add('firstName', 'text', array(
                'label' => 'label.first_name',
                'required' => true
            ))
            ->add('lastName', 'text', array(
                'label' => 'label.last_name'
            ))
            ->add('email', 'text', array(
                'label' => 'label.email'
            ))
            ->add('birthDate', 'date', array(
                'label' => 'label.birth_date',
                'format' => 'd/M/y',
                'input' => 'datetime',
                'widget' => 'single_text',
                'required' => false
            ))
            ->add('phone', 'text', array(
                'label' => 'label.phone',
                'required' => false
            ))
            ->add('address', 'text', array(
                'label' => 'label.address',
                'required' => false
            ))
            ->add('country', 'entity', array(
                'class' => 'CosmedContactBackOfficeBundle:Country',
                'property' => 'name',
                'label' => 'label.country',
                'placeholder' => 'Choisir',
                'empty_data'  => null,
                'required' => false
            ))
            ->add('zipCode', 'text', array(
                'label' => 'label.zip_code',
                'required' => false
            ))
            ->add('city', 'text', array(
                'label' => 'label.city'
            ))
            ->add('company', 'text', array(
                'label' => 'label.company'
            ));
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Cosmed\Contact\BackOfficeBundle\Entity\ContactSender',
        ));
    }

    public function getName()
    {
        return 'cosmed_contact_sender';
    }
} 