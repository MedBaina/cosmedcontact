<?php

namespace Cosmed\Contact\FrontOfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContactReceiverType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('contactReceiver', 'entity', array(
            'class' => 'CosmedContactBackOfficeBundle:ContactReceiver',
            'data_class' => 'Cosmed\Contact\BackOfficeBundle\Entity\ContactReceiver',
            'property' => 'name',
            'label' => 'label.birth_date'));
    }

    public function getName()
    {
        return 'cosmed_contact_receiver';
    }
} 