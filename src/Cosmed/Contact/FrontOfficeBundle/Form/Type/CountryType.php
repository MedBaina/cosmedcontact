<?php

namespace Cosmed\Contact\FrontOfficeBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class CountryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder ->add('country', 'entity', array(
            'class' => 'CosmedContactBackOfficeBundle:ContactReceiver',
            'property' => 'name'
        ));
    }

    public function getName()
    {
        return 'cosmed_country';
    }
} 