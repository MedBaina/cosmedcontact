<?php

namespace Cosmed\Contact\FrontOfficeBundle\Controller;

use Cosmed\Contact\BackOfficeBundle\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class ContactController extends Controller
{
    /**
     * @Route("/cosmed-contact.html")
     * @Template("CosmedContactFrontOfficeBundle:Contact:contact.html.twig")
     *
     * @param \Symfony\Component\HttpFoundation\Request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(Request $request)
    {
        $contact = new Contact();
        $form = $this->container->get('front_office.form.contact');
        $form->setData($contact);
        $form->handleRequest($request);

        if($form->isValid()) {
            $contact = $form->getData();
            $contact->setIp($this->container->get('request')->getClientIp());

            $this->container->get('back_office.contact_manager')->createContact($contact);
            $this->container->get('contact_mailer')->sendMail($contact);
            $this->container->get('session')->getFlashBag()->add('confirmation', 'created');

            return $this->redirectToRoute('cosmed_contact_frontoffice_contact_confirm');
        }

        return array('form' => $form->createView());
    }

    /**
     * @Route("cosmed-contact/confirmation.html")
     * @Template("CosmedContactFrontOfficeBundle:Contact:confirmation.html.twig")
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function confirmAction(Request $request)
    {
        if($this->container->get('session')->getFlashBag()->has('confirmation')) {
            //get Method call on flashBag delete the tag from session
            $this->container->get('session')->getFlashBag()->get('confirmation');

        } else {
            return $this->redirectToRoute('cosmed_contact_frontoffice_contact_index');
        }

    }
}
